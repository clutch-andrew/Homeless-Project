import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.use(Vuetify, {
  iconfont: 'mdi',
  theme: {
    primary: '#ff7a0f',
    secondary: '#fff',
    accent: '#333',
    error: '#b71c1c'
  }
})


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
