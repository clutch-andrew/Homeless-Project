$(document).ready(function() {

	var controller = new ScrollMagic.Controller();

	var fadeOut = new TimelineMax()
		.to('.action', 1, {y: '90', opacity: 0})

	var pinAction = new ScrollMagic.Scene({
		triggerHook: 0,
		duration: '55%'
	})
	.setTween(fadeOut)
	.addTo(controller);

	var menuScene = new ScrollMagic.Scene({
			triggerHook: 0.2,
	    triggerElement: '#help',
		})
		.setClassToggle('.bar', 'newMenu')
	  .addTo(controller);

	var slide = new TimelineMax()
		.to(".slideshow", 1, {x: '-50%'});

	var helpPin = new ScrollMagic.Scene({
			triggerElement: '.pinContainer',
			triggerHook: 'onLeave',
			duration: '100%'
		})
		.setPin(".pinContainer")
		.setTween(slide)
		.addTo(controller);

});
